#include<stdio.h>
#include<math.h>
#include<stdbool.h>
float calcular_hipotenusa(float ca,float cb);
bool es_negativo_float(float numero );

int main (){
    float h=0, c1=0, c2=0;
    do{
        printf("Ingrese el valor de los catetos separados por coma: \n");
         scanf("%f,%f",&c1,&c2);
        if(es_negativo_float(c1) || es_negativo_float(c2)){
         printf("ERROR : Numeros invalidos\n");
        }
    }while (es_negativo_float(c1) || es_negativo_float(c2));
   h=calcular_hipotenusa(c1,c2);
   printf("el valor de la hipotenusa es %2.0f",h);
   return 0;
}

float calcular_hipotenusa(float ca, float cb){
    float hipotenusa=0;
    hipotenusa=sqrt(pow(ca,2)+pow(cb,2));
    return hipotenusa;
}

bool es_negativo_float(float numero ){
    if (numero<0)
    {
        return true;
    }else{
        return false;
    }
    
}